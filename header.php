<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title><?php wp_title(); ?></title>
  <?php wp_enqueue_script('jquery'); ?>
  <?php wp_head(); ?>
  <?php if(is_singular() ) wp_enqueue_script("comment-reply"); ?>
  <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/footer_bottom.js"></script>
  </script>
  <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/style.css">
</head>
<body>
  <div id="allwrp">
    <div id="headerwrp">
      <header>
        <div class="logo"><a href="<?php echo home_url("/");?>">
          <span class="vi">!</span>
          <span class="subtitle"><?php echo bloginfo("description"); ?></span>
          <span class="maintitle">トモタカラボ</span></a>
        </div>
        <div class="header_right">
          <nav class="root">
            <?php wp_nav_menu( array ("theme_location" => "header-navi")); ?>
          </nav>
        </div>
      </header>
    </div>
    <!--header-->
