<?php get_header(); ?>
<div id="wrp">
  <main id="top">
    <article class="general">
      <h2 class="relief">404 Not Found.</h2>
      <section class="wrp_50">
        <p><center>お探しの記事は見つかりませんでした。</center></p>
      </section>
    </article>
  </main>
  <?php get_footer(); ?>
