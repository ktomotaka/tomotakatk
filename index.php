<?php get_header(); ?>
<div id="wrp">
  <main id="top">
    <article class="general">
      <h2 class="relief">当サイトについて</h2>
      <section class="wrp_50">
        <!--
        <p>
        やばい。<br>
        真面目に記事を投稿したら、半日経たずにメールが届きました。<br>
        すごい。アンテナ高すぎる。というか、未だに僕の昔のサイトの名前が、通じるのですか。<br>
        嘘でしょ、マジですか。<br>
        ツクールの世界から、すっかり忘れ去られていたと思っていたのに<br>
        インターネットの世界の、広さと狭さを同時に感じて、センチメンタルな感じになった。<br>
        これは本腰を入れなければ。(`・ω・´)<br>
        <span class="small right">2015年08月08日</span><br>
      </p>
    -->
  </section>
</article>
<article class="postloop">
  <h2 class="relief">さいきん書いたもの</h2>
  <?php if ( have_posts() ) : // 投稿がある場合
    while ( have_posts() ) : the_post(); // 繰り返し処理開始 ?>

    <section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <a href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail(); ?>
        <p><?php echo the_exceprt_cut(); ?></p>
        <h2 style="<?php echo h1_overflow_text_index(); ?>"><?php the_title(); ?></h2>
      </a>
    </section>

  <?php endwhile; ?>
<?php endif; ?>
</article>
<article class="general">
  <h2 class="relief">運営情報と連絡先</h2>
  <section class="wrp_50">
    <dl class="profile">
      <dt>運営者</dt><dd>トモタカ</dd>
      <dt>ひとこと</dt><dd>ニートです。ものつくりがすきです。<br>そろそろ稼がないと、まずいです。</dd>
      <dt>連絡先</dt><dd><a  href="https://docs.google.com/a/tomotaka.biz/forms/d/1GqA52iyie-JyPXv65GK2w_7s-0fXric0zeOLMfdCvAI/viewform" target="_blank">メッセージフォーム</a></dd>
    </dl>
  </section>
</article>
</main>
<?php get_footer(); ?>
