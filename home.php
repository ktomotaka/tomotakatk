<?php get_header(); ?>
<div id="wrp">
  <main id="top">
    <article class="general">
      <h2 class="relief">当サイトについて</h2>
      <section class="wrp_50">
        <p>
          トモタカが作ったものを書いていくサイトです。<br>
          3分で楽しむゲームコンテスト<a href="http://3punge.jp" target="_blank">3分ゲーコンテスト</a>と、RPGツクール専門ニュースサイト<a href="http://tkool.org" target="_blank">ツクール新聞</a>を作っています。
          現在、7年のブランクを経て、復旧準備を進めています。
        </p>
      </section>
    </article>

    <article class="general">
      <h2 class="relief">近況おしらせ</h2>
      <section class="wrp_50">
        <p>
          <span class="strong">ツクールMV関連</span><br>
          最近、RPGツクールMV関連のことばかり書いています。<a href="http://tomotaka.tk/tkoolmv-javascript-studybooks/">初心者ツクラーの為のJavaScript入門</a>、<a href="http://tomotaka.tk/tkoolmv-future/">ツクールMVの未来</a>、<a href="http://tomotaka.tk/rpgtkoolmv-opening-event/">RPGツクールMV発売記念イベント全講演内容</a>、<a href="http://tomotaka.tk/tkoolmv-overview/">RPGツクールMV概要</a>などなど……。<br>
          そろそろ他の事も進めたい、新春です。
        </p>
      <span class="small right">2016年01月07日</span><br>
    </section>

  </article>
  <article class="postloop">
    <h2 class="relief">さいきん書いたもの</h2>
    <?php if ( have_posts() ) : // 投稿がある場合
      while ( have_posts() ) : the_post(); // 繰り返し処理開始 ?>

      <section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <a href="<?php the_permalink(); ?>">
          <?php the_post_thumbnail(); ?>
          <p><?php echo the_exceprt_cut(); ?></p>
          <h2 style="<?php echo h1_overflow_text_index(); ?>">
            <?php the_title(); ?>
          </h2>
        </a>
      </section>

    <?php endwhile; endif ?>
  </article>
  <article class="general">
    <h2 class="relief">運営情報と連絡先</h2>
    <section class="wrp_50">
      <dl class="profile">
        <dt>運営者</dt>
        <dd>トモタカ</dd>
        <dt>ひとこと</dt>
        <dd>
          3分ゲーコンテスト、無事終了しました。第21回は11月中に開きたかったのですが、色々忙しい上に体調も芳しくないので、ちょっと難しそうです……。
        </dd>
        <dt>連絡先</dt><dd><a  href="https://docs.google.com/a/tomotaka.biz/forms/d/1GqA52iyie-JyPXv65GK2w_7s-0fXric0zeOLMfdCvAI/viewform" target="_blank">メッセージフォーム</a></dd>
        <dt>Twitter</dt>
        <dd>
          <a class="twitter-timeline"  href="https://twitter.com/tomotakatk" data-widget-id="631124941182668800">@tomotakatkさんのツイート</a>
          <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
        </dd>
      </dl>

      <!--Googleの軍門に降る-->
      <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <!-- 広告テスト -->
      <ins class="adsbygoogle"
      style="display:block"
      data-ad-client="ca-pub-5137546773114652"
      data-ad-slot="4036575920"
      data-ad-format="auto"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
      <!--Amazonの軍門に降る-->
      <a href="http://c.af.moshimo.com/af/c/click?a_id=533130&p_id=170&pc_id=185&pl_id=4065&guid=ON" target="_blank"><img src="http://image.moshimo.com/af-img/0068/000000004065.gif" width="109" height="28" style="border:none;"></a><img src="http://i.af.moshimo.com/af/i/impression?a_id=533130&p_id=170&pc_id=185&pl_id=4065" width="1" height="1" style="border:none;">

    </section>
  </article>
</main>
<?php get_footer(); ?>
