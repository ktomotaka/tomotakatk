<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
  <div><label class="screen-reader-text" for="s">サイト内検索を行う</label>
    <input type="text" value="" name="s" id="s" required placeholder="サイト内を検索 - Search" />
    <input type="submit" id="searchsubmit" value="(´・ω・`)" />
  </div>
</form>
