<!--関連リンクと広告-->
<section class="link">
  <h2><span class="number">C</span>関連リンクと広告</h2>

  <h3>関連記事</h3>
  <?php echo do_shortcode( '[jetpack-related-posts]' ); ?>

  <h3>共有</h3>
  <?php get_template_part( 'share' ); ?>

  <h3>広告</h3>
  <!--Amazon-->
  <a href="http://c.af.moshimo.com/af/c/click?a_id=533130&p_id=170&pc_id=185&pl_id=4062&url=http%3A%2F%2Fwww.amazon.co.jp%2Fdp%2FB016U2RARM%2F" target="_blank"><img class="center" src="http://tomotaka.tk/data/ad/ad__amazon--728x90.png" alt="RPGツクールMVをAmazonで買う" /></a><img src="http://i.af.moshimo.com/af/i/impression?a_id=533130&p_id=170&pc_id=185&pl_id=4062" width="1" height="1" style="border:none;">

  <!--google adsense-->
  <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
  <!-- tomotakatk_page -->
  <ins class="adsbygoogle"
       style="display:block"
       data-ad-client="ca-pub-5137546773114652"
       data-ad-slot="4607457926"
       data-ad-format="auto"></ins>
  <script>
  (adsbygoogle = window.adsbygoogle || []).push({});
  </script>
</section>

<!--コメント-->
<section class="comments__blog">
  <h2><span class="number">C</span>コメント</h2>
  <p><span class="comment_information">間違いの指摘、タレコミ、感想、批判などなど。なんでも気軽に投稿してください(∩´∀｀)∩<br>直接のご連絡は、<a  href="https://docs.google.com/a/tomotaka.biz/forms/d/1GqA52iyie-JyPXv65GK2w_7s-0fXric0zeOLMfdCvAI/viewform" target="_blank">メッセージフォーム</a>からどうぞ。</span></p>
  <?php
  $comments_args = array(
    "title_reply" => "",
    "comment_notes_before" => "",
    "comment_notes_after" => "",
    "label_submit"         => "コメントを送信する",
    "comment_field" => '<p class="comment-form-comment"><label for="comment">'.
    _x( 'Comment', 'noun' ) .
    '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="コメントを入力してください - Comment"></textarea></p>',

    "fields" => array(
      "author" =>
      '<p class="comment-form-author">'.
      '<label class="input_item" for="author">お名前</label>'.//label tag
      '<input id="author" name="author" type="text" aria-required="true" value="'.
      esc_attr($commenter['comment_author']).//valueの中身。無記名は匿名
      '" './/value tag end
      'size="30" '.
      'placeholder="お名前 - Name"'.//初期入力ヒント
      ' /></p>',
      "email" =>
      '<p class="comment-form-email">'.
      '<label class="input_item" for="email">Eメール</label>'.
      '<input id="email" name="email" type="text" value="'.
      esc_attr($commenter['comment_author_email']).//valueの中身。
      '" './/value tag end
      'size="30" '.
      'placeholder="メールアドレス - Mail"'.//初期入力ヒント
      ' /></p>',
      "url" =>
      '<p class="comment-form-url">'.
      '<label class="input_item" for="url">ウェブサイト</label>'.
      '<input id="url" name="url" type="text" value="'.
      esc_attr($commenter['comment_author_url']).//valueの中身。
      '" './/value tag end
      'size="30" '.
      'placeholder="ウェブサイト - http://... (空欄でもOK)"'.//初期入力ヒント
      ' /></p>',
    ),
  );
  comment_form($comments_args);
  ?>

  <ul class = "commentlist">
    <?php
    $wp_list_comments_args = array(
      "avatar_size" => "40",
      "reply_text" => "返信する",

    );
    wp_list_comments($wp_list_comments_args);
    ?>
  </ul>
</section>
<section class="comments__hatena">
  <div class="hatena-bookmark-marker"><a href="<?php echo get_permalink(); ?>"></a></div>
</section>
