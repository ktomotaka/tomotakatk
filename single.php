<?php get_header(single); ?>
<div id="wrp">
  <main id="page">
    <!--ドキュメントの基本情報-->
    <div id="cover">
      <div class="title" style="<?php echo cover_img(); ?>;">
        <h1 style="<?php echo h1_overflow_text(); ?>"><?php the_title(); ?></h1>
        <div class="writeday">
          <p>
            <time class="create" datetime="<?php echo get_the_date("Y-m-d"); ?>">作成日: <?php echo get_the_date("Y年m月d日"); ?></time><br>
            <time class="update" datetime="<?php echo get_the_modified_date("Y-m-d"); ?>">最終更新日: <?php echo get_the_modified_date("Y年m月d日"); ?></time>
          </p>
        </div>
      </div>

      <div class="docinfo">
        <!--パンくずリスト-->
        <nav class="breadcrumbs">
          <ul>
            <?php if (function_exists('bcn_display')) {bcn_display();}?>
          </ul>
        </nav>
        <nav class="tag">
          <?php the_tags("<ul>\n<li>","</li>\n<li>","</li>\n</ul>"); ?>
        </nav>
      </div>
    </div>

    <!--本文表示-->
    <article>
      <!--要約表示-->
      <h2 class="abstract"><span class="strong">要約</span><br>
        <?php the_excerpt(); ?>
      </h2>

      <!--記事本文-->
      <?php the_content(); ?>

      <!--jetpack-->

      <!--コメント-->
      <?php comments_template(); ?>
    </article>
  </main>
  <?php get_footer(); ?>
