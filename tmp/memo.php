<?php the_content();
$args = array(
    'before'		=> '<div class="page-link">',
    'after'			=> '</div>',
    'link_before'	=> '<span>',
    'link_after'	=> '</span>',
);
wp_link_pages( $args );


<?php get_header(); ?>
<div id="wrp">
  <main>

    <article>
      <section>
        <?php if ( have_posts() ) : // WordPress ループ
          while ( have_posts() ) : the_post(); // 繰り返し処理開始 ?>
          <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            <!--<?php the_content( '続きを読む &raquo;', true ); ?>-->
            <!--<p><?php echo get_bloginfo("description"); ?></p>-->
            <p class="post-meta">
              <?php the_post_thumbnail(); ?>
              <span class="post-date"><?php echo get_the_date(); ?></span><br>
              <span class="category">Category - <?php the_category( ', ' ) ?></span><br>
              <span class="tag">Tag - <?php the_tags(", ") ?></span><br>
              <span class="comment-num"><?php comments_popup_link( 'Comment : 0', 'Comment : 1', 'Comments : %' ); ?></span><br>
            </p>
          </div>
        <?php endwhile; ?>
      </section>
    <?php endif; ?>
  </article>
</main>
<?php get_footer(); ?>


<p class="post-meta">
  <span class="post-date"><?php echo get_the_date(); ?></span><br>
  <span class="category">Category - <?php the_category( ', ' ) ?></span><br>
  <span class="tag">Tag - <?php the_tags(", ") ?></span><br>
  <span class="comment-num"><?php comments_popup_link( 'Comment : 0', 'Comment : 1', 'Comments : %' ); ?></span><br>
</p>

//linear-gradientを実行時に斜めに線をひく
@include linear-gradient(to top left, transparent 50%, #000000 50%, #000000 50.1%, transparent 50.1%);

//@include linear-gradient(transparent 50%, #ccc 50%, #ccc 51%, transparent 51%, transparent 52%, #ccc 52%, transparent 55%);


@keyframes colorful_animation{
  0%{
    text-shadow: -1px 1px 3px rgba(206,41,66,1), 1px 1px 3px rgba(57,108,180,1), 1px -1px 3px rgba(0,157,90,1), -1px -1px 3px rgba(239,182,7,1);
  }
  25%{
    text-shadow: -1px 1px 3px rgba(239,182,7,1), 1px 1px 3px rgba(206,41,66,1), 1px -1px 3px rgba(57,108,180,1), -1px -1px 3px rgba(0,157,90,1);
  }
  50%{
    text-shadow: -1px 1px 3px rgba(0,157,90,1), 1px 1px 3px rgba(239,182,7,1), 1px -1px 3px rgba(206,41,66,1), -1px -1px 3px rgba(57,108,180,1);
  }
  75%{
    text-shadow: -1px 1px 3px rgba(57,108,180,1), 1px 1px 3px rgba(0,157,90,1), 1px -1px 3px rgba(239,182,7,1), -1px -1px 3px rgba(206,41,66,1);
  }
  100%{
    text-shadow: -1px 1px 3px rgba(206,41,66,1), 1px 1px 3px rgba(57,108,180,1), 1px -1px 3px rgba(0,157,90,1), -1px -1px 3px rgba(239,182,7,1);
  }
}
@keyframes monoqlo_animation{
  0%{
    text-shadow: -1px 1px 1px rgba(255,255,255,1), 1px 1px 1px rgba(0,0,0,1), 1px -1px 1px rgba(255,255,255,1), -1px -1px 1px rgba(0,0,0,1);
  }
  25%{
    text-shadow: -1px 1px 1px rgba(0,0,0,1), 1px 1px 1px rgba(255,255,255,1), 1px -1px 1px rgba(0,0,0,1), -1px -1px 1px rgba(255,255,255,1);
  }
  50%{
    text-shadow: -1px 1px 1px rgba(255,255,255,1), 1px 1px 1px rgba(0,0,0,1), 1px -1px 1px rgba(255,255,255,1), -1px -1px 1px rgba(0,0,0,1);
  }
  75%{
    text-shadow: -1px 1px 1px rgba(0,0,0,1), 1px 1px 1px rgba(255,255,255,1), 1px -1px 1px rgba(0,0,0,1), -1px -1px 1px rgba(255,255,255,1);
  }
  100%{
    text-shadow: -1px 1px 1px rgba(255,255,255,1), 1px 1px 1px rgba(0,0,0,1), 1px -1px 1px rgba(255,255,255,1), -1px -1px 1px rgba(0,0,0,1);
  }
}

//中央で分離するグラデーション
@include linear-gradient(transparent 0%,transparent 48%, #eee 50%, #eee 51%, transparent 51%, transparent 52%, #eee 52%, transparent 70%);
