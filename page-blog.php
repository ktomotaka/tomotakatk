<?php get_header(); ?>
<div id="wrp">
  <main id="archive">
    <article class="general">
      <h2 class="relief">投稿記事一覧</h2>
      <!--検索ボックス-->
      <?php get_search_form(); ?>

      <nav class="tag_cloud">
        <?php $args = array(
          'smallest' => 1,
          'largest' => 1,
          'unit' => 'rem',
          'format' => 'list',
        );
        wp_tag_cloud( $args );
        ?>
      </nav>
    </article>

    <article class="postloop">
      <?php query_posts('post_type=post&paged='.$paged); ?>
      <?php if ( have_posts() ) : // 投稿がある場合
        while ( have_posts() ) : the_post();// 繰り返し処理開始 ?>

        <section id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <a href="<?php the_permalink(); ?>">
            <?php the_post_thumbnail(); ?>
            <p><?php echo the_exceprt_cut(); ?></p>
            <h2 style="<?php echo h1_overflow_text_index(); ?>">
              <?php the_title(); ?>
            </h2>
          </a>
        </section>

      <?php endwhile; endif ?>
      <?php wp_reset_query(); ?>
    </article>

  </main>
  <?php get_footer(); ?>
