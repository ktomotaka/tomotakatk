<?php
/*
//the_excerpt();で抜粋を出力時、文字数を制限する。
the_excerpt();はWord Press SEO Yoastによりmeta descriptionにも挿入される。
tomotakatkのテーマ内では、要約(abstract)の部分にthe_exceprtが挿入されます。
*/
/*
function new_excerpt_mblength($length)
{
    return 150;
    add_filter('excerpt_mblength', 'new_excerpt_mblength');
}
*/


//文字を150文字に切り取り
function the_exceprt_cut(){
    $cut_text =  mb_substr(get_the_excerpt(),0,150);
    return $cut_text;
}

//index.phpでタイトルの文字溢れを防ぐ
function h1_overflow_text_index(){
    $title_size = mb_strwidth(get_the_title());//文字列の幅を返す
    if ($title_size > 35) {//文字列幅が32文字以上をtrue
        $font_reduce = 2.7-($title_size*0.02);//デフォルトの2.7emからマイナスする
        $font_reduce = round($font_reduce);//小数点を丸める
        return 'font-size:'.$font_reduce.'em;white-space:nowrap;height:120px;';//styleを返す
    } else {
        return "font-size:2.7em";//何も出力しない
    }
}

//サムネイル機能をONにする
add_theme_support('post-thumbnails');

//ヘッダー背景の出力
function cover_img()
{
    $image_id = get_post_thumbnail_id();//サムネイルIDの取得
    if (has_post_thumbnail()) {//サムネイルが存在するかチェック
        $image_url = wp_get_attachment_image_src($image_id, true);//サムネイルのURLを呼び出す
        return "background:url($image_url[0])";//URLを返す
    } else {//サムネイルが存在しない場合
        return "background:#cccccc";//灰色に設定
    }
}

//adminログイン時に管理ヘッダーを非表示にする
add_filter( 'show_admin_bar', '__return_false' );

//タイトルの文字溢れを防ぐ
function h1_overflow_text()
{
    $title_size = mb_strwidth(get_the_title());//文字列の幅を返す
    if ($title_size > 29) {//文字列幅が31文字以上をtrue
        $font_reduce = 2.7-($title_size*0.025);//デフォルトの2.7emからマイナスする
        return 'font-size:'.$font_reduce.'em';//styleを返す
    } else {
        return "";//何も出力しない
    }
}

//アニメーションを順番に表示させる為のカウントループ
// function anime_loop(){
//     global $wp_query;//グローバル関数の用意
//     $num = $wp_query -> current_post+1;//投稿順番を動かす。
//     return $num*0.1;//アニメーション開始タイミングの計算
// }

//検索時の空文字処理
function empty_search_redirect( $wp_query ) {
	if ( $wp_query->is_main_query() && $wp_query->is_search && ! $wp_query->is_admin ) {
		$s = $wp_query->get( 's' );
		if ( empty( $s ) ) {
			wp_safe_redirect( home_url('/blog') );
			exit;
		}
	}
}
add_action( 'parse_query', 'empty_search_redirect' );

// jetpackの自動挿入の削除
// 関連記事の表示
function jetpackme_remove_rp() {
    $jprp = Jetpack_RelatedPosts::init();
    $callback = array( $jprp, 'filter_add_target_to_dom' );
    remove_filter( 'the_content', $callback, 40 );
}
add_filter( 'wp', 'jetpackme_remove_rp', 20 );

// post-thumbnails
